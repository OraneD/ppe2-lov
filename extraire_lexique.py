def doc_count(liste):
    """
    Cette fonction prend comme argument une liste de chaînes et retourne un dictionnaire 
    associant chaque mot au nombre de documents dans lesquels il apparaît.
    """
    # Initialiser un dictionnaire pour stocker le résultat
    doc_counts = {}
    
    # Découper chaque document de la liste des documents en mots
    for document in liste:
        mots = document.split()
        
        # Incrémenter le compteur d'occurrences de chaque mot
        for mot in mots:
            if mot in doc_counts:
                doc_counts[mot]["occ_mot"] += 1
            else: 
                doc_counts[mot] = {"occ_mot": 1}
                
    # Déterminer le nombre d'apparition de chaque mot dans les documents           
    for mot in doc_counts:
        doc_counts[mot]["occ_doc"] = len([doc for doc in liste if mot in doc.split()])
        
    # Retourner le dictionnaire préalablement initialisé 
    return doc_counts


# Tester la fonction
liste = ["marie aime la musique et la danse", "marie aime la glace au chocolat", "marie aime la glace au chocolat et à la vanille"]
res = doc_count(liste)
print(res)
