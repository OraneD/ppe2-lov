import os
from datastructures import Token
from pathlib import Path
from xml.etree import ElementTree as et
import pandas  as pd
from pprint import pprint
from gensim.models import LdaModel
from gensim.corpora import Dictionary
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
import argparse
import json
import pickle
from gensim.models import Phrases
from typing import List,Optional
df = pd.DataFrame()

#On extrait les lemmes du fichier XML afin de constituer un sac de mots par fichier
def extract_token_xml(fichier, type_token="lemma", pos=[]):
    lst_token_par_article = []
    text =Path(fichier).read_text()
    if len(text) > 0:
        xml = et.parse(fichier)
        root = xml.getroot()
        for article in root.findall(".//analyse"):
            lst_token = []
            previousPos = ""
            isPreviousAdded = False
            for token in article.findall(".//token"):
                if pos == [] or token.get("pos") in pos:
                    if isPreviousAdded and previousPos == token.get("pos"):
                        lst_token[-1] += " " + token.get(type_token)
                    else:
                        lst_token.append(token.get(type_token))
                    isPreviousAdded = True
                    previousPos = token.get("pos")
                else:
                    isPreviousAdded = False
            lst_token_par_article.append(lst_token)
        return lst_token_par_article

#Fonction pour  extraire les tokens et filtrer par pos pour json
def extractJson(fichier,FormeLemme, pos=[]):
    listeAnalyse = []
    with open(fichier, "r") as file:
        data = json.load(file)
        for analyse in range(len(data["articles"])):
            liste = []
            previousPos = ""
            isPreviousAdded = False
            for morpho in range(len(data["articles"][analyse]["analyse"])):
                if pos == [] or data["articles"][analyse]["analyse"][morpho]["pos"] in pos:
                    if isPreviousAdded and previousPos == data["articles"][analyse]["analyse"][morpho]["pos"]:
                        liste[-1] += " " + data["articles"][analyse]["analyse"][morpho][FormeLemme]
                    else:
                        liste.append(data["articles"][analyse]["analyse"][morpho][FormeLemme])
                    isPreviousAdded = True
                    previousPos = data["articles"][analyse]["analyse"][morpho]["pos"]
                else:
                    isPreviousAdded = False
            listeAnalyse.append(liste)
        return listeAnalyse
#test = extractJson("test", "lemme", ["DET","PROPN"])
#print(test)

#Fonction pour l'extraction de type de token pickle + filtre par pos
def extract_token_pickle(fichier, token_type, pos):
    article_tokens_list = []
    with open(fichier, "rb") as f:
        data = pickle.load(f)
        for article in data["articles"]:
            tokens = []
            previousPos = ""
            isPreviousAdded = False
            for token in article["analyse"]:
                if pos == [] or token["pos"] in pos:
                    if isPreviousAdded and previousPos == token["pos"]:
                        tokens[-1] += " " + token[token_type]
                    else:
                        tokens.append(token[token_type])
                    isPreviousAdded = True
                    previousPos = token.get("pos")
                else:
                    isPreviousAdded = False
            article_tokens_list.append(tokens)
        return article_tokens_list



def lda_model(lst_token, topics, freq_min, freq_max) :
    df["Tokens"] = lst_token 
    dictionary = Dictionary(df["Tokens"])
    dictionary.filter_extremes(no_below=freq_min, no_above=freq_max)
    corpus = [dictionary.doc2bow(doc) for doc in df['Tokens']] 
    print('Number of unique tokens: %d' % len(dictionary))
    print('Number of documents: %d' % len(corpus))
    
    # Train LDA model.
    # Set training parameters.
    num_topics = topics
    chunksize = 2000
    passes = 20
    iterations = 400
    eval_every = None  # Don't evaluate model perplexity, takes too much time.
    
    # Make an index to word dictionary.
    temp = dictionary[0]  # This is only to "load" the dictionary.
    id2word = dictionary.id2token
    
    model = LdaModel(
        corpus=corpus,
        id2word=id2word,
        chunksize=chunksize,
        alpha='auto',
        eta='auto',
        iterations=iterations,
        num_topics=num_topics,
        passes=passes,
        eval_every=eval_every
    )
    return corpus, dictionary, model
   # top_topics = model.top_topics(corpus)
    
    # Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
    #avg_topic_coherence = sum([t[1] for t in top_topics]) / topics
   # print('Average topic coherence: %.4f.' % avg_topic_coherence)
    
   # pprint(top_topics)

####################### OUTPUT ########################

   # lda_display = pyLDAvis.gensim_models.prepare(model, corpus, dictionary) #va créer un objet HTML pour la visualisation
    
   # with open("lda.html", "w") as html :
       # html.write(str(pyLDAvis.save_html(lda_display).data))

def save_html_viz(model, corpus, dictionary, output_path):
    # ATTENTION, nécessite pandas en version 1.x
    # pip install pandas==1.5.*
    # (ce qui désinstallera pandas 2 si vous l'avez)
    # (d'où l'intérêt d'avoir un venv par projet) 
    vis_data = gensimvis.prepare(model, corpus, dictionary)
    with open(output_path, "w") as f:
        pyLDAvis.save_html(vis_data, f)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("fichier", help="fichier xml duquel on veut extraire les themes")
    parser.add_argument("-f", help="format du corpus XML, JSON ou PICKLE", default="XML")
    parser.add_argument("-t", help="nombre de topics à extraire", default=10, type=int)
    parser.add_argument("-o", default=None, help="génère la visualisation ldaviz et la sauvegarde dans le fichier html indiqué")
    parser.add_argument("-min", help="occurrence minimum d'un token par document", type=int, default=5)
    parser.add_argument("-max", help="occurrence max d'un token par document (pourcentage)", type=float, default =0.5)
    parser.add_argument("-token", help= "choix des tokens, forme ou lemme", default="lemme")
    parser.add_argument("-pos", nargs="?", help="choix des pos", default=[])
    args = parser.parse_args()
    
    if args.f == "XML" :
        tokens = extract_token_xml(args.fichier, args.token, args.pos)
        c, d, m = lda_model(tokens, args.t, args.min, args.max)
        if args.o is not None : 
            save_html_viz(m, c, d, args.o)
        print("Corpus au format XML, token = " + str(args.token) + " pos = " + str(args.pos) + " nombre de topic = " + str(args.t))
    elif args.f == "JSON" :
        tokens = extractJson(args.fichier, args.token, args.pos)
        c, d, m = lda_model(tokens, args.t, args.min, args.max)
        if args.o is not None : 
            save_html_viz(m, c, d, args.o)
        print("Corpus au format JSON, token = " + str(args.token) + " pos = " + str(args.pos) + " nombre de topic = " + str(args.t))
    elif args.f == "PICKLE" :
        tokens = extract_token_pickle(args.fichier, args.token, args.pos)
        c, d, m = lda_model(tokens, args.t, args.min, args.max)
        if args.o is not None : 
            save_html_viz(m, c, d, args.o)
        print("Corpus au format PICKLE token = " + str(args.token) + " pos = " + str(args.pos) + " nombre de topic = " + str(args.t))
