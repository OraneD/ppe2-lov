#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 11:29:11 2023

@author: orane
"""

import argparse
import xml.etree.ElementTree as ET
from pathlib import Path
def recupTxt(fichiers):
    text = Path(fichiers).read_text()
    if len(text) > 0:
        xml = ET.parse(fichiers)
        root = xml.getroot()
        for item in root.findall(".//item"):
            title = item.find("title").text
            desc = item.find("description").text
            yield title, desc
        
def main()  :
    parser = argparse.ArgumentParser()
    parser.add_argument("fichiers", help="Fichier XML à traiter")
    args = parser.parse_args()
    recupTxt(args.fichiers)
           
        

if __name__=="__main__":
    main()
        

