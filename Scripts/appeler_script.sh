#!/bin/bash
declare -i nbBoucle
echo Nombre de boucle?
read nbBoucle
for (( i=1; i<=$nbBoucle; i++ ))
do
	echo arguments1?
	read args1
	python3 extract_many.py $args1
	echo arguments2?
	read args2
	python3 lda.py $args2
done

# On demande en premier lieu le nombre de boucles qu'on souhaite.
# Ce nombre de boucles va déterminer le nombre de fois où notre script va demander les arguments de nos deux scripts
# Une fois le nombre de boucles donné, on entre les arguments qui correspondent au script 1
# Puis les arguments du script 2 
#Et on recommence tant que le nombre de boucles n'est pas atteint.