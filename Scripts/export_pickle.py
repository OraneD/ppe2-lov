import pickle
from dataclasses import asdict

from datastructures import Corpus, Article, Token

def write_pickle(corpus:Corpus, destination:str):
	with open(destination, "wb") as fout:
            pickle.dump(asdict(corpus), fout)
