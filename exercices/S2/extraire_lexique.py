import sys
import os
import argparse
import pathlib

def string_liste(chemin):
    '''Cette fonction demande un string en argument (ici le dossier Corpus, comme nous sommes placés dans
    le répertoire S2)'''
    liste= []
    corpus = "./" + chemin
    dumps = os.listdir(os.path.join(corpus))
    for d in dumps:
        with open(os.path.join(corpus,d), 'r') as dumps:
            chaine = dumps.read()
            liste.append(chaine)
    return liste


def NbOccurrences(liste) : 
    
  dictionnaire = {}
  
#Nettoyage des données
  for chaine in liste : 
      chaine = chaine.strip()
      chaine = chaine.lower()
#Génération d'une liste de mots pour chaque string dans la liste initiale
      mots = chaine.split(" ")
     
      for mot in mots : 
# Si le mot est déjà dans le dictionnairen alors on augmente sa valeur de 1 
          if mot in dictionnaire :
              dictionnaire[mot] = dictionnaire[mot] +1    
          else :
# Sinon, on le rajoute, avec une valeur de 1 
              dictionnaire[mot] = 1
    
  return dictionnaire


def doc_count(liste):
    """
    Cette fonction prend comme argument une liste de chaînes et retourne un dictionnaire 
    associant chaque mot au nombre de documents dans lesquels il apparaît.
    """
    # Initialiser un dictionnaire pour stocker le résultat
    doc_counts = {}
    
    # Découper chaque document de la liste des documents en mots
    for document in liste:
        mots = document.split()
        
        # Incrémenter le compteur d'occurrences de chaque mot
        for mot in mots:
            if mot in doc_counts:
                doc_counts[mot]["occ_mot"] += 1
            else: 
                doc_counts[mot] = {"occ_mot": 1}
                
    # Déterminer le nombre d'apparition de chaque mot dans les documents           
    for mot in doc_counts:
        doc_counts[mot]["occ_doc"] = len([doc for doc in liste if mot in doc.split()])
        
    # Retourner le dictionnaire préalablement initialisé 
    return doc_counts


# Tester la fonction
#liste = ["marie aime la musique et la danse", "marie aime la glace au chocolat", "marie aime la glace au chocolat et à la vanille"]
#res = doc_count(liste)
#print(res)


def lexique_corpus():
    '''
    Cette fonction ne prend aucun argument.
     Elle premet de rassembler les fonctions des membres du groupe pour
     constituer le lexique de notre corpus.
     Elle renvoie tout de même les résultats de toutes nos fonctions
    '''
    listeCorpus = string_liste("Corpus")
    occurrencesListe = NbOccurrences(listeCorpus)
    compteurOccDoc = doc_count(listeCorpus)
    return listeCorpus,occurrencesListe, compteurOccDoc

#test de la fonction
#test= lexique_corpus()
#print(test)




########################### R1 ###########################

def search_folder(path, extension):
    """
    Recherche des fichiers dans le corpus à traiter
    """
    folder = pathlib.Path(path) #Change de dossier vers celui indiqué dans la ligne de commande
    files = list(folder.rglob(f'*.{extension}')) #Création d'une liste avec le nom de tous les fichiers dans le dossier
    if not files:
        print(f'Pas de fichier avec l\'{extension=}')
        return

    print(f'{len(files)} *.{extension} fichiers trouvés:')
    for file in files:
        lecture = open(file, "r")
        text = lecture.read()
        print( file )
        print(text)
        lecture.close()
        
        
def r1():
    #Création du parser
    parser = argparse.ArgumentParser(
            'extraire_lexique',
            description= "Extraction du lexique pour un corpus donné")
    #ajout d'un argument pour la ligne de commande, ici il s'agit du chemin vers le dossier à traiter
    parser.add_argument('-p',
                        help='Chemin vers le dossier à explorer',
                        required=True,
                        dest='path')
    parser.add_argument('-e',
                        help='Extension recherchée',
                        required=True,
                        dest='extension')
    #Les arguments sont parsés
    args = parser.parse_args()
    #On appelle la fonction qui cherche dans les dossiers avec nos arguments en paramètres
    search_folder(args.path, args.extension)
# Exemple pour lancer le script : python extraire_lexique.py -p Corpus -e txt

############################### R3 #############################
# Exercice R3 semaine 2 Léa

def lecture_fichier_ls(commande) :
    '''fonction prenant en compte un argument qui est l'entrée
    ls Corpus/*.txt |python3  ../../test.py'''

    # Permet au programme de savoir d'où on l'appelle
    currentDirectory = os.getcwd()
    liste=[]
    #l'ajout de [:1] à la variable fichier permet d'enlever \n qui est accollé aux noms de fichiers
    for fichier in commande:
        with open(currentDirectory+"/"+fichier[:-1], 'r') as dumps:
            chaine = dumps.read()
            liste.append(chaine)
    print(liste)
    return liste

commande = sys.stdin

#lecture_fichier_ls(commande)

############################### R2 #############################
def lire_corpus_r2():
    """
    Cette fonction va permettre de lire le corpus depuis l'entrée standard
    """

    # Lire le contenu depuis l'entrée standard
    corpus = sys.stdin.read()

    # Transformer les fichiers du corpus en une liste de fichiers texte
    fichiers = corpus.split('\0')

    # Lire la totalité des fichiers dans le corpus
    texte_corpus = []
    for fichier in fichiers:
        with open(fichier, 'r') as f:
            texte_corpus.append(f.read())

    return texte_corpus

def mode_lecture():
# Cette fonction va lancer une des trois fonctions pour lire le corpus en fonction des arguments de la ligne de commande
# Ça ne fonctionne pas car "ls" et "cat" ne sont pas des arguments.. 
# Seul r1 se lance correctement.

    print(sys.argv)
    if "ls" in sys.argv:
        print(lecture_fichier_ls(commande))
    elif "-p" in sys.argv :
        print(r1())
    elif "cat" in sys.argv : 
       print(lire_corpus_r2())

mode_lecture()

    
