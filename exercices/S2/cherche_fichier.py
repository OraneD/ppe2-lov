#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 19 18:46:19 2023

@author: orane
"""

import argparse
import pathlib


def search_folder(path, extension):
    """
    Recherche des fichiers dans le corpus à traiter
    """
    folder = pathlib.Path(path) #Change de dossier vers celui indiqué dans la ligne de commande
    files = list(folder.rglob(f'*.{extension}')) #Création d'une liste avec le nom de tous les fichiers dans le dossier
    if not files:
        print(f'Pas de fichier avec l\'{extension=}')
        return

    print(f'{len(files)} *.{extension} fichiers trouvés:')
    for file in files:
        lecture = open(file, "r")
        text = lecture.read()
        print( file )
        print(text)
        lecture.close()
        
        
def main():
    #Création du parser
    parser = argparse.ArgumentParser(
            'cherche_fichier',
            description='Ce script permet de chercher un type de fichier dans un dossier',
            )
    #ajout d'un argument pour la ligne de commande, ici il s'agit du chemin vers le dossier à traiter
    parser.add_argument('-p',
                        help='Chemin vers le dossier à explorer',
                        required=True,
                        dest='path')
    parser.add_argument('-e',
                        help='Extension recherchée',
                        required=True,
                        dest='extension')
    #Les arguments sont parsés
    args = parser.parse_args()
    #On appelle la fonction qui cherche dans les dossiers avec nos arguments en paramètres
    search_folder(args.path, args.extension)
    
if __name__ == '__main__':
    main()

# Exemple pour lancer le script : python cherche_fichier.py -p Corpus -e txt