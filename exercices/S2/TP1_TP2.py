#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 10 01:12:54 2023

@author: orane
"""
import sys
import argparse
from pathlib import Path
from typing import List, Dict
import errno

#--------------Commandes pour appeler les fonctions-----------#
#r1 -> python TP1_TP2.py -r1 Corpus/*.txt
#r2 -> cat Corpus/*.txt | python TP1_TP2 -r2
#r3 -> Corpus/*.txt | python TP1_TP2.py -r3
#On peut rajouter --help à la fin de chaque appel pour savoir ce que l'option r1 r2 ou r3 fait

def lire_corpus_r1(fichiers: List[str])-> List[str]:
    resultat = []
    for fichier in fichiers:
        texte = Path(fichier).read_text("utf-8")
        resultat.append(texte)
    return resultat


def lire_corpus_r2(): # Utiliser script convert_corpus.sh avant de lancer celle ci
    resultat = []
    for text in sys.stdin :
        resultat.append(text)
    return resultat
    
def lire_corpus_r3():
    resultat = []
    for fichier in sys.stdin: #chemin vers des fichiers
        texte = Path(fichier.strip()).read_text("utf-8")
        resultat.append(texte)
    return resultat


def term_freq(corpus: List[str]) -> Dict[str,int]:
    resultat = {}
    for doc in corpus:
        for word in doc.split():
            if word in resultat:
                resultat[word] += 1
            else:
                resultat[word] = 1
    return resultat

def doc_freq(corpus: List[str]) -> Dict[str,int]:
    resultat = {}
    for doc in corpus:
        words = set(doc.split())
        for word in words:
            if word in resultat:
                resultat[word] += 1
            else:
                resultat[word] = 1
    return resultat




def main():
#-----------------R1----------------#
    parser = argparse.ArgumentParser()
    parser.add_argument("fichiers", help="fichiers à lire comme documents du corpus", nargs="*")
    parser.add_argument("-r1", action="store_true", help="on lit les fichiers donnés en argument")
    parser.add_argument("-r2", action="store_true", help="on lit directement le corpus depuis stdin (un doc par ligne)")
    parser.add_argument("-r3", action="store_true", help="on lit les chemins de fichiers depuis stdin")
    args = parser.parse_args()

    if args.r1 and len(args.fichiers)>0: #si on a l'argument r1 on appelle fonction r1 et qu'on a au moins un fichier
        corpus = lire_corpus_r1(args.fichiers)
    if args.r2:
        corpus = lire_corpus_r2()
    if args.r3:
        corpus = lire_corpus_r3()
       
    print("doc freq")
    for k, v in doc_freq(corpus).items():
        print(f"{k}: {v}")
    print("term freq")
    for k, v in term_freq(corpus).items():
        print(f"{k}: {v}")
    
    

if __name__ == "__main__":
    try:
        main()
    except IOError as e: # si une erreur de ce type survient
        if e.errno == errno.EPIPE: # on continue l'ex du programme quand même
            pass
    Path("./fini.txt").touch() #Quand le programme a fini il créé un fichier fini.txt
